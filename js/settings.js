async function hash(message) {
    const msgUint8 = new TextEncoder().encode(message); // encode as (utf-8) Uint8Array
    const hashBuffer = await crypto.subtle.digest("SHA-256", msgUint8); // hash the message
    const hashArray = Array.from(new Uint8Array(hashBuffer)); // convert buffer to byte array
    const hashHex = hashArray
      .map((b) => b.toString(16).padStart(2, "0"))
      .join(""); // convert bytes to hex string
    return hashHex;
}

window.addEventListener("message", evnt=> {
    details = JSON.parse(evnt.data)
    document.getElementById("cusername").innerHTML = details["username"]
    document.getElementById("picture").src = details["picture"]
})
window.parent.postMessage("userdetails", "*")

function updatepfp() {
    flpicker = document.createElement("input")
    flpicker.type = "file"
    flpicker.addEventListener("change", ()=> {
        flreader = new FileReader()
        flreader.onload = function (evnt) {
            data = Array.from(new Uint8Array(evnt.target.result))
            window.parent.postMessage( JSON.stringify( {"picture": data} ), "*") 
            console.log()
        }
        flreader.readAsArrayBuffer(flpicker.files[0])
    })
    flpicker.click()
}
function save() {
    let username = document.getElementById("editusername").value
    if (username) {
        window.parent.postMessage( JSON.stringify( {"username": username} ), "*")
    }
    let password = document.getElementById("editpassword").value
    if(password) {
        hash(password, passhash => {
            window.parent.postMessage(JSON.stringify( {"passhash": passhash}, "*"))
        })
    }
    close()
}
function close(){
    console.log("closing")
    window.parent.postMessage("CLOSE", "*")
}
function logout() {
    window.parent.postMessage("LOGOUT", "*")
}