function open_popup(htmlpath) {
    let popupdiv = document.createElement("div");
    popupdiv.setAttribute("id", "popupdiv")
    let popup = document.createElement("iframe")
    popup.src = htmlpath
    popup.style.zIndex = 100
    popup.setAttribute("class", "popupwindow")
    let bkrd = document.createElement("div");
    bkrd.setAttribute("class", "popupbackground")
    popupdiv.appendChild(bkrd)
    popupdiv.appendChild(popup)
    document.body.appendChild(popupdiv)
    return popup
}

if(!localStorage.getItem("server")) {
    console.log("No server IP was found, opening iframe...")
    open_popup("html/server_set.html")
    window.addEventListener("message", evnt => {
        console.log(evnt.data)
        localStorage.setItem("server", evnt.data)
        location.reload()
    })
    throw Error("Awaiting server IP...")
}
if(!localStorage.getItem("campfire_user") || !localStorage.getItem("campfire_passhash")) {
    console.log("Username or password was not found, opening iframe prompt...")
    open_popup("html/userpass.html")
    window.addEventListener("message", evnt => {
        userpass = JSON.parse(evnt.data)
        username = userpass["username"]
        passhash = userpass["passhash"]
        localStorage.setItem("campfire_user", username)
        localStorage.setItem("campfire_passhash", passhash)
        location.reload()
    })
    throw Error("Awaiting username and password...")
}
var inTab = true;
var unseen_msg_count = 0;
var timeFormat = "{STANDARDHOUR}:{MINUTES} {STANDARDSUFFIX}";
var username = localStorage.getItem("campfire_user");
let apiserver = "http://" + localStorage.getItem("server") + ":5000"
let ws = new WebSocket("ws://" + localStorage.getItem("server") + ":5001");
let currentTab = "Crossroads";
document.getElementById("currentTab").innerHTML = currentTab
var pictures = {}
atBottom = false
var msglists = {}
var holdingShift = false

window.addEventListener("keydown", evnt => {
    let key = evnt.key
    if(key == "Shift") {
        holdingShift = true
    } else if(key == "Enter" && !holdingShift) {
        send()
        evnt.preventDefault()
    }
})
window.addEventListener("keyup", evnt => {
    let key = evnt.key
    if(key == "Shift") {
        holdingShift = false
    }
})

document.getElementById("mainpfp").src = apiserver + "/picture?user=" + username
ws.onmessage = function(msg) {
    msg = msg.data
    console.log(msg)
    if(msg === "OK") {
        console.log("Successfully authenticated");
        ws.onmessage = function(msg) {
            console.log(msg.data)
            for(let message of JSON.parse(msg.data)) {
                createmessage(message)
            }
            ws.onmessage = message_handler;
            ws.send(JSON.stringify( {"message": "LISTUSERS", "type": "text", "recipiant": "", "commandargs": ["Crossroads"]} ))
            ws.send(JSON.stringify( {"message": "LISTCAMPSITES", "type": "text", "recipiant": "", "commandargs": ["Crossroads"]} ))
        }
        ws.send(JSON.stringify( {"message": "PREVIOUSMSG", "type": "text", "recipiant": "", "commandargs": ["Crossroads", 50]} ))
        setInterval(refresh_users, 10000)
    } else {
        signout(false)
    }
}
ws.onopen = function() {
    console.log("Opened..")
    ws.send(JSON.stringify({"username": username, "passhash": localStorage.getItem("campfire_passhash")}))
}

addEventListener("visibilitychange", evnt => {
	if(document.visibilityState == "visible") {
		inTab = true;
		document.title = "Campfire"
		unseen_msg_count = 0
	} else {
        holdingShift = false;
		inTab = false;
	}
})

function message_handler(msg) {
    msg = JSON.parse(msg.data);
    if(msg["Error"]) {
        alert(msg["Error"])
    }
    sender = msg["sender"];
    type = msg["type"];
    msgdata = msg["message"];
    if(type == "SYSTEMMESSAGE") {
        if(msg["onlineusers"]) {
            document.getElementById("onlineusers").innerHTML = ""
            msg["onlineusers"].forEach(user => {
                console.log(user)
                create_right_user(user)
            })
        } else if(msg["users"]) {
            create_left_dms(msg["users"])
        } else if(msg["campsites"]) {
            console.log("Loading campsites")
            create_left_campsites(msg["campsites"])
        }
    } else {
			if(!inTab) {
				unseen_msg_count += 1
				document.title = "(" + unseen_msg_count + ") Campfire"
			}
        createmessage(msg)
    } 
    /*
    } else if(type.includes("image")) {
        let image = document.createElement("img")
        image.src = msgdata
    } else if(type.includes("video")) {
        let video = document.createElement("video")
        video.src = msgdata
    } else {
        console.log(type)
        console.log("Recieved unknown type of message.")
        return;
    }*/
}

document.getElementById("messagelist").addEventListener("scroll", evnt => {
    msglist = document.getElementById("messagelist")
    atBottom = !(msglist.scrollTop != msglist.scrollTopMax)
})


function createmessage(msg, nosave) {
    let sender = msg["sender"]
    let msgdata = msg["message"]
    let type = msg["type"]
		let time = timeText(msg);
    let destination = ""
    if(msg["campsite"]) {
        destination = msg["campsite"]
    } else {
        destination = currentTab
    }
    if(!pictures[sender]) {
        pictures[sender] = apiserver + "/picture?user=" + sender
    }
    console.log(!nosave)
    if(currentTab != destination) {
        console.log("dest:", destination, "tab:",currentTab)
        if(!msglists[destination]) {
            msglists[destination] = []
        }
        msglists[destination].push(msg)
        return
    }
    if(!nosave) {
        if(!msglists[destination]) {
            msglists[destination] = []
        }
        msglists[destination].push(msg)
    }
    console.log(currentTab, destination)
    let msgdiv = document.createElement("div")
    //Time
    let time_element = document.createElement("p");
    time_element.innerHTML = time
    time_element.setAttribute("class", "timetext")
		if(time_element.innerHTML.includes("(?)")) {
			time_element.title = "The time on this message is based on when you recieved the message, not the server. This may be incorrect."
		}
    msgdiv.appendChild(time_element)

    let pfp_element = document.createElement("img")
    pfp_element.setAttribute("class", "pfp")
    pfp_element.src = pictures[sender]
    msgdiv.appendChild(pfp_element)
    let msg_element = document.createElement("p");
    msg_element.innerHTML = sender + ": ";
		msg_element.style.marginLeft = "5px"
    if(type == "message" || type == "text") {
        msg_element.innerHTML += msgdata.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\n/g, "<br>")
        for(let embed of create_embed(msgdata)) {
            msgdiv.appendChild(embed)
        }
				if(msgdata.includes("@" + username)) {
					document.getElementById("notifelement").play()
					msgdiv.style.backgroundColor = "#88C0D0"
					msg_element.style.color = "#2E3440"
				}
        msgdiv.appendChild(msg_element)
    } else {
        msgdiv.appendChild(msg_element)
        msgdiv.appendChild(create_element_message(msgdata, type))
    }
    msgdiv.setAttribute("class", "message")
    msglist = document.getElementById("messagelist")
    msglist.appendChild(msgdiv)
    if(!atBottom) {
        msglist.scrollTo(0, msglist.scrollHeight)
    }
}

function timeText(msg) {
	
	let date;
	let returnValue = timeFormat;
	if (!msg["time"]) {
		date = new Date()
		returnValue += " (?)"
	} else {
		date = new Date(msg["time"])
	}
	if (returnValue.includes("{STANDARDHOUR}")) {
		if (date.getHours() > 12) {
			returnValue = returnValue.replace(/{STANDARDHOUR}/g, date.getHours() - 12);
		} else {
			returnValue = returnValue.replace(/{STANDARDHOUR}/g, date.getHours())
		}
	}
	if(returnValue.includes("{STANDARDSUFFIX}")) {
		if (date.getHours() > 12) {
			returnValue = returnValue.replace(/{STANDARDSUFFIX}/g, "PM");
		} else {
			returnValue = returnValue.replace(/{STANDARDSUFFIX}/g, "AM")
		}
	}
	if(returnValue.includes("MINUTES")) {
		if(date.getMinutes() > 9) {
			returnValue = returnValue.replace(/{MINUTES}/g, date.getMinutes())
		} else {
			returnValue = returnValue.replace(/{MINUTES}/g, "0" + date.getMinutes())
		}
	}
	return returnValue.replace(/{MILITARYHOURS}/g, date.getHours()).replace(/{DAY}/g, date.getDay()).replace(/{MONTH}/g, date.getMonth())
}

function create_embed(msgstring) {
    embeds = []
    allowstring = "picture-in-picture;"
    msgstring.split(" ").forEach(word => {
        let url;
        try {
            url = new URL(word)
        } catch(_) {
            return
        }
        if(!url) {return}
        if(["music.youtube.com", "www.youtube.com"].includes(url.hostname)){
            embed = document.createElement("iframe");
            embed.src = "https://youtube.com/embed/" + url.searchParams.get("v")
            embed.allow = "picture-in-picture;"
            embed.setAttribute("class", "embed")
            embed.allowFullScreen = true;
            embeds.push(embed)
        }

    })
    return embeds
}

function create_element_message(data, type) {
    let element;
    if(type.includes("image")) {
        element = document.createElement("img")
        element.style.maxHeight = "800px"
        element.style.maxWidth = "800px"
        element.src = "data:" + type + ";base64," + data
    } else if(type.includes("audio")) {
        element = document.createElement("audio")
        element.src = "data:" + type + ";base64," + data
        element.controls = true;
    } else if(type.includes("video")) {
        element = document.createElement("video")
        element.src = "data:" + type + ";base64," + data
        element.controls = true;
        element.style.maxHeight = "800px"
        element.style.maxWidth = "800px"
    }
    console.log(element)
    return element
}

function create_right_user(username) {
    let user_element = document.createElement("p");
    user_element.innerHTML = username + "<br>";
    user_element.setAttribute("id", "right_" + username);
    document.getElementById("onlineusers").appendChild(user_element);
}

function create_left_dms(users) {
    let div = document.getElementById("dms")
    users.forEach(user => {
        let userbtn = document.createElement("button")
        userbtn.setAttribute("class", "leftbuttons")
        userbtn.onclick = function() {changeTab(user)}
        let pfp = document.createElement("img");
        pfp.src = apiserver + "/picture?user=" + user
        pfp.setAttribute("class", "pfp leftpfp")
        userbtn.appendChild(pfp)
        let username_element = document.createElement("p");
        username_element.innerHTML = user;
        username_element.setAttribute("class", "lefttext")
        userbtn.appendChild(username_element)
        div.appendChild(userbtn)
        div.appendChild(document.createElement("br"))
    })
}

function create_left_campsites(campsites) {
    let div = document.getElementById("campsites");
    campsites.forEach(campsite => {
        let campsitebtn = document.createElement("button");
        let campsitetext = document.createElement("p");
        campsitetext.innerHTML = campsite
        campsitebtn.onclick = function() {changeTab(campsite)}
        campsitebtn.appendChild(campsitetext)
        div.appendChild(campsitebtn)
        div.appendChild(document.createElement("br"))
    })
}

function send() {
    if (document.getElementById("inputfield").value.trim() == "") {
        console.log("Cannot send blank messages.")
    }else {
    console.log("sending")
    messagebox = document.getElementById("inputfield");
    message = messagebox.value
    console.log(message)
    ws.send( JSON.stringify( {"message": message, "type": "text", "recipiant": currentTab} ) )
    messagebox.value = ""
    }
}
function sendfl(fl, type) {
    console.log(fl, type)
    console.log("sending")
    ws.send( JSON.stringify( {"message": fl, "type": type, "recipiant": currentTab} ) )
}
function refresh_users() {
    console.log("sending")
    ws.send( JSON.stringify( {"message": "LISTONLINEUSERS", "recipiant": "server", "type": "text", "commandargs": ["Crossroads"]} ) )
}

function settings() {
    let settings_window = open_popup("html/settings.html");
    window.addEventListener("message", evnt => {
        if(evnt.data == "userdetails") {
            settings_window.contentWindow.postMessage(JSON.stringify({"username": username, "picture": pictures[username]}), "*")
            return
        } else if(evnt.data == "CLOSE") {
            document.body.removeChild(document.getElementById("popupdiv"))
            return
        } else if(evnt.data == "LOGOUT") {
            signout(true)
        } 
        try {
            data = JSON.parse(evnt.data)
            ws.send(JSON.stringify( {"message": "SETTINGS", "recipiant": "", "type": "text", "commandargs": [data]} ))
        } catch(err) {
            console.log(err)
        }
    })
}
function changeTab(user) {
    currentTab = user
    document.getElementById("messagelist").innerHTML = ""
    document.getElementById("currentTab").innerHTML = currentTab
    for (let msg of msglists[user]){
        createmessage(msg, true)
    }
    msglist = document.getElementById("messagelist")
    msglist.scrollTo(0, msglist.scrollTopMax)

}
document.addEventListener("drop", evnt => {
    evnt.preventDefault()
    for(let i = 0; i < evnt.dataTransfer.files.length; i++){
        fl = evnt.dataTransfer.files[i]
        if(fl.type == "") {
            type = "application/octet-stream"
        } else {
            type = fl.type
        }
        flreader = new FileReader()
        flreader.onload = read => {
            sendfl(Array.from( new Uint8Array(read.target.result)), type)
        }
        flreader.readAsArrayBuffer(fl)
    }
})

function open_file() {
    let inp = document.createElement("input")
    inp.type = "file"
    inp.click()
    inp.onchange = evnt => {
        if(inp.files.length == 0) {
            return
        } else {
            for(let i = 0; i < inp.files.length; i++){
                fl = inp.files[i]
                console.log(fl)

                if(fl.type == "") {
                    type = "application/octet-stream"
                } else {
                    type = fl.type
                }
                flreader = new FileReader()
                flreader.onload = read => {
                    sendfl(Array.from( new Uint8Array(read.target.result)), type)
                }
                flreader.readAsArrayBuffer(fl)
            }
            
        }
    }
}

function signout(removeServer) {
    localStorage.removeItem("campfire_user")
    localStorage.removeItem("campfire_passhash")
    if(removeServer) {

        localStorage.removeItem("server")
    }
    location.reload()
}
